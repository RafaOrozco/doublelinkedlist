package cursocoritel.doublelinkedlist;

/**
 * 
 * @author Rafael Orozco Almario
 *
 * @param <T>
 */
public class DoubleLinkedList<T> {

	private Node<T> firstNode;
	private Node<T> lastNode;

	/**
	 * Constructs an empty DoubleLinkedList
	 */
	public DoubleLinkedList() {
		this.firstNode = null;
		this.lastNode = null;
	}

	/**
	 * Insert a node after a given node.
	 * 
	 * @param node
	 * @param newNode
	 */
	public void insertAfter(Node<T> node, Node<T> newNode) {
		newNode.setPrev(node);
		if (!node.hashNext()) {
			lastNode = newNode;
		} else {
			newNode.setNext(node.getNext());
			node.getNext().setPrev(newNode);
		}
		node.setNext(newNode);
	}

	/**
	 * Insert a node before a given node.
	 * 
	 * @param node
	 * @param newNode
	 */
	public void insertBefore(Node<T> node, Node<T> newNode) {
		newNode.setNext(node);
		if (!node.hashPrev()) {
			firstNode = newNode;
		} else {
			newNode.setPrev(node.getPrev());
			node.getPrev().setNext(newNode);
		}
		node.setPrev(newNode);
	}

	/**
	 * Insert a node at the beginning of a possibly empty list.
	 * 
	 * @param newNode
	 */
	public void insertBeginning(Node<T> newNode) {
		if (firstNode == null) {
			firstNode = newNode;
			lastNode = newNode;
			newNode.setPrev(null);
			newNode.setNext(null);
		} else {
			insertBefore(firstNode, newNode);
		}
	}

	/**
	 * Insert a node at the end of a possibly empty list.
	 * 
	 * @param newNode
	 */
	public void insertEnd(Node<T> newNode) {
		if (lastNode == null) {
			insertBeginning(newNode);
		} else {
			insertAfter(lastNode, newNode);
		}
	}
/**
 * Remove a given node
 * @param node
 */
	public void remove(Node<T> node) {
		if (!node.hashPrev()) {
			firstNode = node.getNext();
		} else {
			node.getPrev().setNext(node.getNext());
		}

		if (!node.hashNext()) {
			lastNode = node.getPrev();
		} else {
			node.getNext().setPrev(node.getPrev());
		}
	}

	/**
	 * Nodes separated by commas
	 * 
	 * @return A list representation
	 */
	public String print() {
		StringBuilder string=new StringBuilder();
		Node<T> node = firstNode;
		while (node != null) {
			string.append(node.getData());
			if (node.hashNext()) {
			string.append(",");
			}
			node = node.getNext();
			
		}
		return string.toString();
	}

	/**
	 * 
	 * @return the first node
	 */
	public Node<T> getFirstNode() {
		return firstNode;
	}

	/**
	 * 
	 * @return the last node
	 */
	public Node<T> getLastNode() {
		return lastNode;
	}

}
