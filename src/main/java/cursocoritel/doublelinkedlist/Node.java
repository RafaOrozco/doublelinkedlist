package cursocoritel.doublelinkedlist;

/**
 * 
 * @author Rafael Orozco Almario
 *
 * @param <T>
 */
public class Node<T> {

	private Node<T> prev;
	private Node<T> next;
	private T data;

	/**
	 * 
	 * @param prev
	 * @param next
	 * @param data
	 */
	public Node(Node<T> prev, Node<T> next, T data) {
		this.prev = prev;
		this.next = next;
		this.data = data;
	}

	/**
	 * 
	 * @param data
	 */
	public Node(T data) {
		this(null, null, data);
	}

	/**
	 * 
	 * @return
	 */
	public Node<T> getPrev() {
		return prev;
	}

	/**
	 * 
	 * @param prev
	 */
	public void setPrev(Node<T> prev) {
		this.prev = prev;
	}

	/**
	 * 
	 * @return
	 */
	public Node<T> getNext() {
		return next;
	}

	/**
	 * 
	 * @param next
	 */
	public void setNext(Node<T> next) {
		this.next = next;
	}

	/**
	 * 
	 * @return
	 */
	public T getData() {
		return data;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((data == null) ? 0 : data.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object other) {
		return other instanceof Node<?> && data.equals(((Node<?>) other).data);
	}

	/**
	 * true if has next false otherwise
	 * 
	 * @return
	 */
	public boolean hashNext() {
		return next != null;
	}

	/**
	 * true if has prev false otherwise
	 * 
	 * @return
	 */
	public boolean hashPrev() {
		return prev != null;
	}

}
