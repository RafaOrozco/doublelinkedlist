package cursocoritel.doublelinkedlist;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import cursocoritel.doublelinkedlist.DoubleLinkedList;
import cursocoritel.doublelinkedlist.Node;

/**
 * 
 * @author Rafael Orozco Almario
 *
 */
public class DoubleLinkedListTest {
	private DoubleLinkedList<Integer> list;

	/**
	 * Initializes the list
	 */
	@Before
	public void setUp() {
		list = new DoubleLinkedList<>();
	}

	/**
	 * Destroys the list
	 */
	@After
	public void tearDown() {
		list = null;
	}

	/**
	 * The inserted node should be the first node
	 */
	@Test
	public void insertBeginningInAnEmptyList() {
		Node<Integer> node = new Node(1);
		list.insertBeginning(node);
		assertEquals(node, list.getFirstNode());
	}

	/**
	 * The inserted node should be the first node
	 */
	@Test
	public void insertBegginingOfInANotEmptyList() {
		Node<Integer> node = new Node(1);
		list.insertBeginning(node);
		Node<Integer> newNode = new Node(2);
		list.insertBeginning(newNode);
		assertEquals(newNode, list.getFirstNode());
	}

	/**
	 * The inserted node should be before a given node and not to be the first
	 * node
	 */
	@Test
	public void insertBeforeInTheMiddle() {
		Node<Integer> node1 = new Node(1);
		Node<Integer> node2 = new Node(2);
		Node<Integer> newNode = new Node(3);
		list.insertBeginning(node2);
		list.insertBeginning(node1);
		list.insertBefore(node2, newNode);
		assertEquals(newNode, node2.getPrev());
		assertNotEquals(newNode, list.getFirstNode());
	}

	/**
	 * The inserted node should be the last node
	 */
	@Test
	public void insertAfterInAnEmptyList() {
		Node<Integer> node = new Node(1);
		list.insertEnd(node);
		assertEquals(node, list.getLastNode());
	}

	/**
	 * The inserted node should be the last node
	 */
	@Test
	public void insertAfterOfInANotEmptyList() {
		Node<Integer> node = new Node(1);
		list.insertEnd(node);
		Node<Integer> newNode = new Node(2);
		list.insertEnd(newNode);
		assertEquals(newNode, list.getLastNode());

	}

	/**
	 * The inserted node should be after a given node and not to be the last
	 * node
	 */
	@Test
	public void insertAFterInTheMiddle() {
		Node<Integer> node1 = new Node(1);
		Node<Integer> node2 = new Node(2);
		Node<Integer> newNode = new Node(3);
		list.insertEnd(node2);
		list.insertEnd(node1);
		list.insertAfter(node2, newNode);
		assertEquals(newNode, node2.getNext());
		assertNotEquals(newNode, list.getLastNode());
	}

	/**
	 * Remove the only element of a list. The list should be empty
	 */
	@Test
	public void removeThefirstOneElementList() {
		Node<Integer> node = new Node(1);
		list.insertBeginning(node);
		list.remove(node);
		assertNull(list.getFirstNode());
	}

	/**
	 * The first node should be the next
	 */
	@Test
	public void removeTheFirstOfAThreeElementList() {
		Node<Integer> node1 = new Node(1);
		Node<Integer> node2 = new Node(2);
		Node<Integer> node3 = new Node(3);
		list.insertBeginning(node3);
		list.insertBeginning(node2);
		list.insertBeginning(node1);
		list.remove(node1);
		assertEquals(list.getFirstNode(), node2);
	}

	/**
	 * The first node next should be the last
	 */
	@Test
	public void removeTheMiddleOfAThreeElementList() {
		Node<Integer> node1 = new Node(1);
		Node<Integer> node2 = new Node(2);
		Node<Integer> node3 = new Node(3);
		list.insertBeginning(node3);
		list.insertBeginning(node2);
		list.insertBeginning(node1);
		list.remove(node2);
		assertEquals(list.getFirstNode().getNext(), node3);
	}

	/**
	 * The last node should be the middle
	 */
	@Test
	public void removeTheLastOfAThreeElementList() {
		Node<Integer> node1 = new Node(1);
		Node<Integer> node2 = new Node(2);
		Node<Integer> node3 = new Node(3);
		list.insertBeginning(node3);
		list.insertBeginning(node2);
		list.insertBeginning(node1);
		list.remove(node3);
		assertEquals(list.getLastNode(), node2);
	}

	/**
	 * Print a list
	 */
	@Test
	public void printTest() {
		String string = "1,2,3";
		Node<Integer> node1 = new Node(1);
		Node<Integer> node2 = new Node(2);
		Node<Integer> node3 = new Node(3);
		list.insertBeginning(node3);
		list.insertBeginning(node2);
		list.insertBeginning(node1);
		assertEquals(string, list.print());
	}

	/**
	 * Print only one element
	 */
	@Test
	public void printOneElement() {
		String string = "1";
		Node<Integer> node1 = new Node(1);
		list.insertBeginning(node1);
		assertEquals(string, list.print());
	}
	
	
	/**
	 * Print an empty element
	 */
	@Test
	public void printEmptyList() {
		String string = "";
		assertEquals(string, list.print());
	}
	/**
	 * Test hashcode of Node
	 */
	@Test
	public void hashCodeTest() {
		Node<Integer> node1 = new Node(1);
		Node<Integer> node2 = new Node(1);
		assertEquals(node1.hashCode(),node2.hashCode());
	}
	
	/**
	 * Test hashcode of  null Node
	 */
	@Test
	public void hashCodeTest0() {
		Node<Integer> node=new Node(null);
		assertEquals(31,node.hashCode());
	}
}
